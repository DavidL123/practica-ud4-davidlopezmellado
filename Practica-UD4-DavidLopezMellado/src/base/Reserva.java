package base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Reserva {


    private ObjectId id;
    private LocalDate fechaInicioReserva;
    private LocalDate fechaFinReserva;
    private float precioTotal;

    public Reserva(){

    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public LocalDate getFechaInicioReserva() {
        return fechaInicioReserva;
    }

    public void setFechaInicioReserva(LocalDate fechaInicioReserva) {
        this.fechaInicioReserva = fechaInicioReserva;
    }

    public LocalDate getFechaFinReserva() {
        return fechaFinReserva;
    }

    public void setFechaFinReserva(LocalDate fechaFinReserva) {
        this.fechaFinReserva = fechaFinReserva;
    }

    public float getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(float precioTotal) {
        this.precioTotal = precioTotal;
    }

    @Override
    public String toString() {
        return
                "Id= " + id +"  "+
                "Fecha inicio reserva= " + fechaInicioReserva +"  "+
                "Fecha fin reserva= " + fechaFinReserva +"  "+
                "Precio total= " + precioTotal +"  ";
    }
}
