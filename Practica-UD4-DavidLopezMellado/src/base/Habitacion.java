package base;

import org.bson.types.ObjectId;

public class Habitacion {


    private ObjectId id;
    private int numeroHabitacion;
    private String tipo;
    private float precioNoche;
    private String vistas;

    public Habitacion(){

    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public int getNumeroHabitacion() {
        return numeroHabitacion;
    }

    public void setNumeroHabitacion(int numeroHabitacion) {
        this.numeroHabitacion = numeroHabitacion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getPrecioNoche() {
        return precioNoche;
    }

    public void setPrecioNoche(float precioNoche) {
        this.precioNoche = precioNoche;
    }

    public String getVistas() {
        return vistas;
    }

    public void setVistas(String vistas) {
        this.vistas = vistas;
    }


    @Override
    public String toString() {
        return
                "Id= " + id + "  "+
                "Número habitación= " + numeroHabitacion + "  "+
                "Tipo= " + tipo + "  " +
                "Precio noche= " + precioNoche +"  "+
                "Vistas= " + vistas+"  ";
    }
}
