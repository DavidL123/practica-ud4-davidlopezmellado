package base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Cliente {

    private ObjectId id;
    private String nombre;
    private String apellidos;
    private LocalDate fechaNacimiento;
    private String domicilio;
    private String telefono;

    public Cliente(){

    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }


    @Override
    public String toString() {
        return
                "Id= " + id +
                " Nombre= " + nombre + "  " +
                " Apellidos= " + apellidos + "  " +
                " Fecha nacimiento= " + fechaNacimiento +
                " Domicilio= " + domicilio + "  " +
                " Telefono= " + telefono+"  " ;
    }
}
