package gui;

import base.Cliente;
import base.Habitacion;
import base.Reserva;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

public class Vista extends JFrame{
    private JPanel panel1;
     JTextField txtClienteNombre;
     JTextField txtApellidosCliente;
     JTextField txtDomicilioCliente;
     JTextField txtTelefonoCliente;
     JTextField txtNumeroHabitacion;
     JTextField txtTipoHabitacion;
     JTextField txtPrecioNoche;
     JTextField txtPrecioTotalReserva;
    JTextField txtVistasHabitacion;
     JButton btnNuevoCliente;
     JButton btnModificarCliente;
     JButton btnEliminarCliente;
     JButton btnNuevoHabitacion;
     JButton btnModificarHabitacion;
     JButton btnEliminarHabitacion;
     JButton btnNuevoReserva;
     JButton btnModificarReserva;
     JButton btnEliminarReserva;
     DatePicker datePickerFechaNacimientoCliente;
     DatePicker datePickerFechaInicioReserva;
     DatePicker datePickerFechaFinReserva;
    JTextField txtBuscarCliente;
    JTextField txtBuscarHabitacion;
    JTextField txtBuscarReserva;
     JList listCliente;
     JList listReserva;
     JList listHabitacion;
    JList listBusquedaHabitacion;
    JList listBusquedaReserva;
     JList listBusquedaCliente;

    DefaultListModel<Cliente> dlmCliente;
    DefaultListModel<Habitacion> dlmHabitacion;
    DefaultListModel<Reserva> dlmReserva;
    DefaultListModel<Cliente> dlmClienteBusqueda;
    DefaultListModel<Habitacion> dlmHabitacionBusqueda;
    DefaultListModel<Reserva> dlmReservaBusqueda;

    JMenuItem itemConectar;
    JMenuItem itemSalir;
    public Vista() {
        JFrame frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //frame.setPreferredSize(new Dimension(700, 550));
        frame.setResizable(false);
        frame.pack();
        frame.setVisible(true);

        inicializar();
        inicializarMenu(frame );
    }
    private void inicializar(){
        dlmCliente = new DefaultListModel<Cliente>();
        listCliente.setModel(dlmCliente);

        dlmHabitacion= new DefaultListModel<Habitacion>();
        listHabitacion.setModel(dlmHabitacion);

        dlmReserva= new DefaultListModel<Reserva>();
        listReserva.setModel(dlmReserva);

        dlmClienteBusqueda= new DefaultListModel<Cliente>();
        listBusquedaCliente.setModel(dlmClienteBusqueda);

        dlmHabitacionBusqueda= new DefaultListModel<Habitacion>();
        listBusquedaHabitacion.setModel(dlmHabitacionBusqueda);

        dlmReservaBusqueda=new DefaultListModel<Reserva>();
        listBusquedaReserva.setModel(dlmReservaBusqueda);



    }

    private void inicializarMenu(JFrame frame) {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("conexionDesconexion");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("salir");

        JMenu menuArchivo = new JMenu("Opciones");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemSalir);

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(menuArchivo);

        frame.setJMenuBar(menuBar);
    }
}
