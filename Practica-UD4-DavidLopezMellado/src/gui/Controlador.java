package gui;

import base.Cliente;
import base.Habitacion;
import base.Reserva;
import util.Util;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

public class Controlador implements ActionListener, KeyListener, ListSelectionListener {
    Vista vista;
    Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        inicializar();
    }

    private void inicializar() {
        addActionListeners(this);
        addKeyListeners(this);
        addListSelectionListeners(this);
        try{
            modelo.conectar();
            vista.itemConectar.setText("Desconectar");
        } catch (Exception ex) {
            Util.mostrarMensajeError("No se ha podido conectar con el servidor");
        }
        listarClientes(modelo.getClientes());
        listarHabitaciones(modelo.getHabitaciones());
        listarReservas(modelo.getReservas());

        setBotonesActivados(true);

    }


    private void addActionListeners(ActionListener listener){
        vista.btnNuevoCliente.addActionListener(listener);
        vista.btnModificarCliente.addActionListener(listener);
        vista.btnEliminarCliente.addActionListener(listener);

        vista.btnNuevoHabitacion.addActionListener(listener);
        vista.btnModificarHabitacion.addActionListener(listener);
        vista.btnEliminarHabitacion.addActionListener(listener);

        vista.btnNuevoReserva.addActionListener(listener);
        vista.btnModificarReserva.addActionListener(listener);
        vista.btnEliminarReserva.addActionListener(listener);

        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
    }



    private void addListSelectionListeners(ListSelectionListener listener){
        vista.listCliente.addListSelectionListener(listener);
        vista.listReserva.addListSelectionListener(listener);
        vista.listHabitacion.addListSelectionListener(listener);
    }


    private void addKeyListeners(KeyListener listener){

        vista.txtBuscarCliente.addKeyListener(listener);
        vista.txtBuscarHabitacion.addKeyListener(listener);
        vista.txtBuscarReserva.addKeyListener(listener);

    }


    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        Cliente unCliente;
        Habitacion unaHabitacion;
        Reserva unaReserva;
        switch (comando){
            case "NuevoCliente":
                unCliente=new Cliente();
                modificarClienteFromCampos(unCliente);
                modelo.guardarCliente(unCliente);
                listarClientes(modelo.getClientes());
                break;
            case "ModificarCliente":
                unCliente= (Cliente) vista.listCliente.getSelectedValue();
                modificarClienteFromCampos(unCliente);
                modelo.modificarCliente(unCliente);
                listarClientes(modelo.getClientes());
                break;
            case "EliminarCliente":
                unCliente= (Cliente) vista.listCliente.getSelectedValue();
                modelo.borrarCliente(unCliente);
                listarClientes(modelo.getClientes());
                break;

            case "NuevoHabitacion":
                unaHabitacion=new Habitacion();
                modificarHabitacionFromCampos(unaHabitacion);
                modelo.guardarHabitacion(unaHabitacion);
                listarHabitaciones(modelo.getHabitaciones());
                break;
            case "ModificarHabitacion":
                unaHabitacion= (Habitacion) vista.listHabitacion.getSelectedValue();
                modificarHabitacionFromCampos(unaHabitacion);
                modelo.modificarHabitacion(unaHabitacion);
                listarHabitaciones(modelo.getHabitaciones());
                break;
            case "EliminarHabitacion":
                unaHabitacion= (Habitacion) vista.listHabitacion.getSelectedValue();
                modelo.borrarHabitacion(unaHabitacion);
                listarHabitaciones(modelo.getHabitaciones());
                break;
            case "NuevoReserva":
                unaReserva=new Reserva();
                modificarReservaFromCampos(unaReserva);
                modelo.guardarReserva(unaReserva);
                listarReservas(modelo.getReservas());
                break;
            case "ModificarReserva":
                unaReserva= (Reserva) vista.listReserva.getSelectedValue();
                modificarReservaFromCampos(unaReserva);
                modelo.modificarReserva(unaReserva);
                listarReservas(modelo.getReservas());
                break;
            case "EliminarReserva":
                unaReserva= (Reserva) vista.listReserva.getSelectedValue();
                modelo.borrarReserva(unaReserva);
                listarReservas(modelo.getReservas());
                break;
            case "conexionDesconexion":
                try {
                    if (modelo.getMongoCliente() == null) {
                        modelo.conectar();
                        limpiarTodosCampos();
                        setBotonesActivados(true);
                        vista.itemConectar.setText("Desconectar");
                        listarClientes(modelo.getClientes());
                        listarHabitaciones(modelo.getHabitaciones());
                        listarReservas(modelo.getReservas());
                    } else {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        vista.dlmCliente.clear();
                        vista.dlmHabitacion.clear();
                        vista.dlmReserva.clear();
                        vista.dlmClienteBusqueda.clear();
                        vista.dlmHabitacionBusqueda.clear();
                        vista.dlmReservaBusqueda.clear();
                        limpiarTodosCampos();
                        setBotonesActivados(false);
                    }
                } catch (Exception ex) {
                    Util.mostrarMensajeError("Imposible establecer conexión con el servidor.");
                }

                break;
            case "salir":
                System.exit(0);
                break;
        }
    }

    private void limpiarTodosCampos() {
        //Cliente
        vista.txtClienteNombre.setText("");
        vista.txtApellidosCliente.setText("");
        vista.datePickerFechaNacimientoCliente.setDate(null);
        vista.txtDomicilioCliente.setText("");
        vista.txtTelefonoCliente.setText("");

        //Habitacion
        vista.txtNumeroHabitacion.setText("");
        vista.txtTipoHabitacion.setText("");
        vista.txtPrecioNoche.setText("");
        vista.txtVistasHabitacion.setText("");

        //Reserva
        vista.datePickerFechaInicioReserva.setDate(null);
        vista.datePickerFechaFinReserva.setDate(null);
        vista.txtPrecioTotalReserva.setText("");


    }

    private void listarClientes(List<Cliente> lista){
        vista.dlmCliente.clear();
        for (Cliente cliente: lista) {
            vista.dlmCliente.addElement(cliente);
        }
    }

    private void listarBusquedaClientes(List<Cliente> lista){
        vista.dlmClienteBusqueda.clear();
        for (Cliente cliente: lista) {
            vista.dlmClienteBusqueda.addElement(cliente);
        }
    }
    private void listarHabitaciones(List<Habitacion> lista){
        vista.dlmHabitacion.clear();
        for (Habitacion habitacion: lista) {
            vista.dlmHabitacion.addElement(habitacion);
        }
    }

    private void listarBusquedaHabitaciones(List<Habitacion> lista){
        vista.dlmHabitacionBusqueda.clear();
        for (Habitacion habitacion: lista) {
            vista.dlmHabitacionBusqueda.addElement(habitacion);
        }
    }
    private void listarReservas(List<Reserva> lista){
        vista.dlmReserva.clear();
        for (Reserva reserva: lista) {
            vista.dlmReserva.addElement(reserva);
        }
    }

    private void listarBusquedaReservas(List<Reserva> lista){
        vista.dlmReservaBusqueda.clear();
        for (Reserva reserva: lista) {
            vista.dlmReservaBusqueda.addElement(reserva);
        }
    }

    private void modificarClienteFromCampos(Cliente unCliente){
        unCliente.setNombre(vista.txtClienteNombre.getText());
        unCliente.setApellidos(vista.txtApellidosCliente.getText());
        unCliente.setFechaNacimiento(vista.datePickerFechaNacimientoCliente.getDate());
        unCliente.setDomicilio(vista.txtDomicilioCliente.getText());
        unCliente.setTelefono(vista.txtTelefonoCliente.getText());
    }
    private void modificarHabitacionFromCampos(Habitacion unaHabitacion){
        unaHabitacion.setNumeroHabitacion(Integer.parseInt(vista.txtNumeroHabitacion.getText()));
        unaHabitacion.setTipo(vista.txtTipoHabitacion.getText());
        unaHabitacion.setPrecioNoche(Float.parseFloat(vista.txtPrecioNoche.getText()));
        unaHabitacion.setVistas(vista.txtVistasHabitacion.getText());
    }
    private void modificarReservaFromCampos(Reserva unaReserva){
        unaReserva.setFechaInicioReserva(vista.datePickerFechaInicioReserva.getDate());
        unaReserva.setFechaFinReserva(vista.datePickerFechaFinReserva.getDate());
        unaReserva.setPrecioTotal(Float.parseFloat(vista.txtPrecioTotalReserva.getText()));

    }

    private void setBotonesActivados(boolean activados) {
        vista.btnNuevoCliente.setEnabled(activados);
        vista.btnModificarCliente.setEnabled(activados);
        vista.btnEliminarCliente.setEnabled(activados);
        vista.btnNuevoReserva.setEnabled(activados);
        vista.btnModificarReserva.setEnabled(activados);
        vista.btnEliminarReserva.setEnabled(activados);
        vista.btnNuevoHabitacion.setEnabled(activados);
        vista.btnModificarHabitacion.setEnabled(activados);
        vista.btnEliminarHabitacion.setEnabled(activados);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){

            if(e.getSource() == vista.listCliente) {
                Cliente unCliente = (Cliente) vista.listCliente.getSelectedValue();
                vista.txtClienteNombre.setText(unCliente.getNombre());
                vista.txtApellidosCliente.setText(unCliente.getApellidos());
                vista.datePickerFechaNacimientoCliente.setDate(unCliente.getFechaNacimiento());
                vista.txtDomicilioCliente.setText(unCliente.getDomicilio());
                vista.txtTelefonoCliente.setText(unCliente.getTelefono());
            }

            if(e.getSource() == vista.listHabitacion) {
                Habitacion unaHabitacion= (Habitacion) vista.listHabitacion.getSelectedValue();
                vista.txtNumeroHabitacion.setText(String.valueOf(unaHabitacion.getNumeroHabitacion()));
                vista.txtTipoHabitacion.setText(unaHabitacion.getTipo());
                vista.txtPrecioNoche.setText(String.valueOf(unaHabitacion.getPrecioNoche()));
                vista.txtVistasHabitacion.setText(unaHabitacion.getVistas());
            }

            if(e.getSource() == vista.listReserva) {
                Reserva unaReserva = (Reserva) vista.listReserva.getSelectedValue();
                vista.datePickerFechaInicioReserva.setDate(unaReserva.getFechaInicioReserva());
                vista.datePickerFechaFinReserva.setDate(unaReserva.getFechaFinReserva());
                vista.txtPrecioTotalReserva.setText(String.valueOf(unaReserva.getPrecioTotal()));
            }

        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

        if (e.getSource() == vista.txtBuscarCliente) {
            listarBusquedaClientes(modelo.getClientes(vista.txtBuscarCliente.getText()));
            if (vista.txtBuscarCliente.getText().isEmpty()) {
                vista.dlmClienteBusqueda.clear();
            }
        }

/*
        if (e.getSource() == vista.txtBuscarHabitacion) {
            listarBusquedaClientes(modelo.getHabitaciones(vista.txtBuscarHabitacion.getText()));
            if (vista.txtBuscarHabitacion.getText().isEmpty()) {
                vista.dlmHabitacionBusqueda.clear();
            }
        }
*/
        if (e.getSource() == vista.txtBuscarHabitacion) {
            listarBusquedaHabitaciones(modelo.getHabitaciones(vista.txtBuscarHabitacion.getText()));
            if (vista.txtBuscarHabitacion.getText().isEmpty()) {
                vista.dlmHabitacionBusqueda.clear();
            }
        }

        if (e.getSource() == vista.txtBuscarReserva) {
            listarBusquedaReservas(modelo.getReservas(vista.txtBuscarReserva.getText()));
            if (vista.txtBuscarReserva.getText().isEmpty()) {
                vista.dlmReservaBusqueda.clear();
            }
        }

        /*
        if (e.getSource() == vista.txtBuscarHabitacion) {
            listarHabitaciones(modelo.getCoches(vista.txtBuscarHabitacion.getText()));
        }


        if (e.getSource() == vista.txtBuscarReserva) {
            listarReservas(modelo.getCoches(vista.txtBuscarReserva.getText()));
        }
        */

    }


}
