package gui;

import base.Cliente;
import base.Habitacion;
import base.Reserva;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import util.Util;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Modelo {

    private final static String COLECCION_CLIENTES ="Clientes";
    private final static String COLECCION_HABITACIONES ="Habitaciones";
    private final static String COLECCION_RESERVAS ="Reservas";
    private final static String DATABASE="Hotel";

    private MongoClient client;
    private MongoDatabase baseDatos;
    private MongoCollection<Document> coleccionClientes;
    private MongoCollection<Document> coleccionHabitaciones;
    private MongoCollection<Document> coleccionReservas;


    public void conectar() {
        client =new MongoClient();
        baseDatos=client.getDatabase(DATABASE);
        coleccionClientes=baseDatos.getCollection(COLECCION_CLIENTES);
        coleccionHabitaciones=baseDatos.getCollection(COLECCION_HABITACIONES);
        coleccionReservas=baseDatos.getCollection(COLECCION_RESERVAS);
    }


    public MongoClient getMongoCliente() {
        return client;
    }

    public void desconectar(){
        client.close();
        client = null;
    }

    public void guardarCliente(Cliente unCliente) {
        coleccionClientes.insertOne(clienteToDocument(unCliente));
    }


    public List<Cliente> getClientes(){
        ArrayList<Cliente> lista = new ArrayList<>();

        Iterator<Document> it =coleccionClientes.find().iterator();
        while (it.hasNext()) {
            lista.add(documentToCliente(it.next()));
        }
        return lista;
    }


    public List<Cliente> getClientes(String text) {
        ArrayList<Cliente> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios =  new ArrayList<>();
        listaCriterios.add(new Document("nombre",new Document("$regex","/*"+text+"/*")));
        listaCriterios.add(new Document("apellidos",new Document("$regex","/*"+text+"/*")));
        listaCriterios.add(new Document("fechaNacimiento",new Document("$regex","/*"+text+"/*")));
        listaCriterios.add(new Document("domicilio",new Document("$regex","/*"+text+"/*")));
        listaCriterios.add(new Document("telefono",new Document("$regex","/*"+text+"/*")));
        query.append("$or",listaCriterios);
        Iterator<Document> iterator = coleccionClientes.find(query).iterator();
        while (iterator.hasNext()) {
            lista.add(documentToCliente(iterator.next()));
        }
        return lista;
    }

    public Document clienteToDocument(Cliente unCliente){
        Document documento = new Document();
        documento.append("nombre",unCliente.getNombre());
        documento.append("apellidos",unCliente.getApellidos());
        documento.append("fechaNacimiento", Util.formatearFecha(unCliente.getFechaNacimiento()));
        documento.append("domicilio",unCliente.getDomicilio());
        documento.append("telefono",unCliente.getTelefono());
        return documento;
    }

    public Cliente documentToCliente(Document document){
        Cliente unCliente =  new Cliente();
        unCliente.setId(document.getObjectId("_id"));
        unCliente.setNombre(document.getString("nombre"));
        unCliente.setApellidos(document.getString("apellidos"));
        unCliente.setFechaNacimiento(Util.parsearFecha(document.getString("fechaNacimiento")));
        unCliente.setDomicilio(document.getString("domicilio"));
        unCliente.setTelefono(document.getString("telefono"));
        return unCliente;
    }

    public void modificarCliente(Cliente unCliente) {
        coleccionClientes.replaceOne(new Document("_id",unCliente.getId()),
                clienteToDocument(unCliente));
    }

    public void borrarCliente(Cliente unCliente) {
        coleccionClientes.deleteOne(clienteToDocument(unCliente));
    }


    //HABITACIONES
    public void guardarHabitacion(Habitacion unaHabitacion) {
        coleccionHabitaciones.insertOne(habitacionToDocument(unaHabitacion));
    }


    public List<Habitacion> getHabitaciones(){
        ArrayList<Habitacion> lista = new ArrayList<>();

        Iterator<Document> it =coleccionHabitaciones.find().iterator();
        while (it.hasNext()) {
            lista.add(documentToHabitacion(it.next()));
        }
        return lista;
    }


    public List<Habitacion> getHabitaciones(String text) {
        ArrayList<Habitacion> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios =  new ArrayList<>();
        listaCriterios.add(new Document("numeroHabitacion",new Document("$regex","/*"+text+"/*")));
        listaCriterios.add(new Document("tipo",new Document("$regex","/*"+text+"/*")));
        listaCriterios.add(new Document("precioNoche",new Document("$regex","/*"+text+"/*")));
        listaCriterios.add(new Document("vistas",new Document("$regex","/*"+text+"/*")));
        query.append("$or",listaCriterios);
        Iterator<Document> iterator = coleccionHabitaciones.find(query).iterator();
        while (iterator.hasNext()) {
            lista.add(documentToHabitacion(iterator.next()));
        }
        return lista;
    }

    public Document habitacionToDocument(Habitacion unaHabitacion){
        Document documento = new Document();
        documento.append("numeroHabitacion",unaHabitacion.getNumeroHabitacion());
        documento.append("tipo",unaHabitacion.getTipo());
        documento.append("precioNoche", unaHabitacion.getPrecioNoche());
        documento.append("vistas",unaHabitacion.getVistas());
        return documento;
    }

    public Habitacion documentToHabitacion(Document document){
        Habitacion unaHabitacion =  new Habitacion();
        unaHabitacion.setId(document.getObjectId("_id"));
        unaHabitacion.setNumeroHabitacion(document.getInteger("numeroHabitacion"));
        unaHabitacion.setTipo(document.getString("tipo"));
        unaHabitacion.setPrecioNoche(Float.parseFloat(String.valueOf(document.getDouble("precioNoche"))));
        unaHabitacion.setVistas(document.getString("vistas"));
        return unaHabitacion;
    }

    public void modificarHabitacion(Habitacion unaHabitacion) {
        coleccionHabitaciones.replaceOne(new Document("_id",unaHabitacion.getId()),
                habitacionToDocument(unaHabitacion));
    }

    public void borrarHabitacion(Habitacion unaHabitacion) {
        coleccionHabitaciones.deleteOne(habitacionToDocument(unaHabitacion));
    }



    //RESERVAS
    public void guardarReserva(Reserva unaReserva) {
        coleccionReservas.insertOne(reservaToDocument(unaReserva));
    }


    public List<Reserva> getReservas(){
        ArrayList<Reserva> lista = new ArrayList<>();

        Iterator<Document> it =coleccionReservas.find().iterator();
        while (it.hasNext()) {
            lista.add(documentToReserva(it.next()));
        }
        return lista;
    }


    public List<Reserva> getReservas(String text) {
        ArrayList<Reserva> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios =  new ArrayList<>();
        listaCriterios.add(new Document("fechaInicioReserva",new Document("$regex","/*"+text+"/*")));
        listaCriterios.add(new Document("fechaFinReserva",new Document("$regex","/*"+text+"/*")));
        listaCriterios.add(new Document("precioTotal",new Document("$regex","/*"+text+"/*")));
        query.append("$or",listaCriterios);
        Iterator<Document> iterator = coleccionReservas.find(query).iterator();
        while (iterator.hasNext()) {
            lista.add(documentToReserva(iterator.next()));
        }
        return lista;
    }

    public Document reservaToDocument(Reserva unaReserva){
        Document documento = new Document();
        documento.append("fechaInicioReserva",Util.formatearFecha(unaReserva.getFechaInicioReserva()));
        documento.append("fechaFinReserva",Util.formatearFecha(unaReserva.getFechaFinReserva()));
        documento.append("precioTotal", unaReserva.getPrecioTotal());
        return documento;
    }

    public Reserva documentToReserva(Document document){
        Reserva unaReserva =  new Reserva();
        unaReserva.setId(document.getObjectId("_id"));
        unaReserva.setFechaInicioReserva(Util.parsearFecha(document.getString("fechaInicioReserva")));
        unaReserva.setFechaFinReserva(Util.parsearFecha(document.getString("fechaFinReserva")));
        unaReserva.setPrecioTotal(Float.parseFloat(String.valueOf(document.getDouble("precioTotal"))));
        return unaReserva;
    }

    public void modificarReserva(Reserva unaReserva) {
        coleccionReservas.replaceOne(new Document("_id",unaReserva.getId()),
                reservaToDocument(unaReserva));
    }

    public void borrarReserva(Reserva unaReserva) {
        coleccionReservas.deleteOne(reservaToDocument(unaReserva));
    }
}
